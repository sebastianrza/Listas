if __name__ == "__main__":
    #Crear lista
    Lista1 = ['bah','beh','bih','boh','buh']
    print(Lista1)

    #recorrer a la lista
    print(Lista1)

    for elemento in Lista1:
        print(elemento)

    #compara lista
    Lista2 = ['beh', 'bah', 'boh', 'buh', 'bih']

    if Lista1==Lista2:
        print("Son identicas")

    #longitud de lista
    print(len(Lista1))

    #concatenar lista
    print(Lista1 + Lista2)

    #Numeros Maximo y minimo
    Lista3 = [1,2,3,4,5]
    print(max(Lista3))
    print(min(Lista3))

    #Lista Enlazada
    Lista4 = ['Esto es', [1,2,3], 75]
    print(Lista4)
    print(Lista4[0])
    print(Lista4[1])
    print(Lista4[1][2])

    #Manipular datos

    Lista5 = ['o', 'p', 'a']
    Lista5.append('z')#Agregar al final de la lista
    print(Lista5)
    Lista5.insert(0,'x')#Insertar en el lugar 0 la x
    Lista5.extend(Lista3)#Fusiona lista 3 con la 5
    print(Lista5)
    Lista5.remove('z')#Eliminar un lugar elegido
    print(Lista5)

    for i in range(5): #=(i=0;i<5;i++
        Lista5.pop()#Eliminar el ultimo de la lista
    print(Lista5)

    Lista5[0]='s'
    print(Lista5)

    #replicar lista

    Lista6 = [1,2,3]
    print(Lista6 * 2)

    #Slicing

    Lista7 = ['carimañola', 'pajarilla', 'bofe']
    print(Lista7[1:2])

    Lista8 = []

    for i in range (100):
        Lista8.append(i)

    print(Lista8[6:70])
    print(Lista8[:51])
    print(Lista8[25:])


